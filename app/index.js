const express = require('express');
const app = express();
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');



const port = process.env.PORT || config.app.port;;
module.exports = class Application {
    constructor() {
        this.setupExpress();
        this.setMongoConnection();
        this.setConfig();
        this.setRouters();
    }

    setupExpress() {
        const server = http.createServer(app);
        server.listen(port, () => console.log('Listening on port ' + port));
        //server.timeout=2000000;
        //server.keepAliveTimeout = 60000 * 2;

    }

    setMongoConnection() {
       
    }

    /**
     * Express Config
     */
    setConfig() {

        app.use(express.static('public'));
        app.set('view engine', 'ejs');
        app.set('views', path.resolve('./resource/views'));

        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
    
    }

    setRouters() {
        app.use(require('app/routes/web'));
    }
}
